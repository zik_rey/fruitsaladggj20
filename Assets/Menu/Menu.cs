﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public string game="";
    public void PlayPressed()
    {
        Debug.Log("Enter");
        SceneManager.LoadScene(game);

    }
    public void ExitPressed()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
