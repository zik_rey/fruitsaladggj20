﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region PrivateData

    [SerializeField] private Transform _playerPosition; 
    [SerializeField] private Vector3 _cameraOffset = new Vector3 (0,0,-100); 
    [SerializeField] private float _smoothSpeed = 0.125f;

    #endregion


    #region UnityMethods

    void Awake()
    {
        _playerPosition = GameObject.FindGameObjectWithTag("Main Hero").transform;
    }

    void LateUpdate()
    {
        if (_playerPosition)
        {
            Vector3 desiredPos = _playerPosition.position + _cameraOffset;
            Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, _smoothSpeed);
            transform.position = smoothedPos;
        }
    }

    #endregion
}
