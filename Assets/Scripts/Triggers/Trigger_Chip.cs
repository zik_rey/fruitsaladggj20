﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Chip : MonoBehaviour
{
    public Level level;
    public Animator animator;
    private void OnTriggerEnter(Collider col)
    {        
        if (col.gameObject.name == "leg")
        {
            Debug.Log("Body and leg");
            animator.SetTrigger("Comics1");
            level.is_con = true;
        }
    }
}
