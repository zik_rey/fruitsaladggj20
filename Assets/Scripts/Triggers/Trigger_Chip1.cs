﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Chip1 : MonoBehaviour
{
    public Animator animator;
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Robot")
        {
            Debug.Log("Chip2");
            animator.SetTrigger("Comics3");
        }
    }
}
