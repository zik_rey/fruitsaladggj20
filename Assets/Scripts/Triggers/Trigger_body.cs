﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_body : MonoBehaviour
{
    public GameObject leg;
    public GameObject body;
    public GameObject new_body;
    public Transform new_body_t;
    public Player new_body_p;
    public CameraControl camera_;

    public Animator animator;
  
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "leg")
        {
            
            animator.SetTrigger("Comics2");
            Destroy(leg);
            Destroy(body);
            camera_.player = new_body_t;
            new_body_p.Is_control = true;
            new_body.SetActive(true);

        }
    }
}
