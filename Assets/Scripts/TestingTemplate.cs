﻿using UnityEngine;

public class TestingTemplate : MonoBehaviour
{
    #region PrivateData

    #endregion


    #region Fields
    public float _distanse = 10;
    public Vector2 dir = new Vector2(0, 1);

    #endregion


    #region UnityMethods

    void Start()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, _distanse);
        Debug.Log($"{ hit}, {hit==null}");
    }

    void Update()
    {
        
    }

    #endregion


    #region Methods

    #endregion
}
