﻿using UnityEngine;

public class DoorController : MonoBehaviour
{
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

   
    void Start()
    {
        CloseDoor();
    }


    public void OpenDoor()
    {
        _animator.SetBool("Open", true);
    }

    public void CloseDoor()
    {
        _animator.SetBool("Open", false);
    }

    public void SwitchDoorState()
    {
        _animator.SetBool("Open", !_animator.GetBool("Open"));
    }
}
