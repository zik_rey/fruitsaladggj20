﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    [SerializeField] private bool _isMoving;
    [SerializeField] private bool _isLoopingMooving;
    [SerializeField] private bool _isVertical;
    [SerializeField] private bool _useDelay;
    [SerializeField] private float _delay = 1f;
    [SerializeField] private float _speed = 0.2f;
    [SerializeField] private Rigidbody _platformRigidbody;
    [SerializeField] private Vector3 newPos;
    [SerializeField] private string _borderTag = "Border";
    private float _currentDelay;

    
    void Start()
    {
        
    }

    private void Update()
    {
        Cooldown();
    }


    void FixedUpdate()
    {
        if (_isMoving)
        {
            MoveVertical();
            MoveHorizontal();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_borderTag))
        {
            Debug.Log($"OnTriggerEnter - execute");
            if (_isLoopingMooving)
            {
                _currentDelay = _delay;
                _speed *= -1;
            }
            else
            {
                _isMoving = false;
            }
        }
    }

    private void Cooldown()
    {
        if (_useDelay)
        {
            if (_currentDelay > 0)
            {
                _currentDelay -= Time.deltaTime;
                _isMoving = false;
            }
            else
            {
                _currentDelay = 0;
                _isMoving = true;
            }
        }
    }

    private void MoveVertical()
    {
        if (_isVertical)
        {
            newPos = Vector3.right * _speed;
            _platformRigidbody.velocity = newPos;
        }
    }

    private void MoveHorizontal()
    {
        if (!_isVertical)
        {
            newPos = Vector3.up * _speed;
            _platformRigidbody.velocity = newPos;
        }

        

    }

    //private void DrowRays()
    //{
    //    RaycastHit2D _leftFocus = Physics2D.Raycast()

    //    RaycastHit2D _leftFocus = Physics2D.Raycast(transform.position, Vector2.left, _rayDistance, _playerMask);
    //    RaycastHit2D _rightFocus = Physics2D.Raycast(transform.position, Vector2.right, _rayDistance, _playerMask);

    //}

}
