﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField] Image[] playersImages;
    [SerializeField] Image selectionImage;
    [SerializeField] GameObject playersPanel;
    [SerializeField] GameObject limbsPanel;
    public int selectedPlayer = 0;
    private void Start()
    {
        playersPanel.gameObject.SetActive(false);
        limbsPanel.gameObject.SetActive(false);
    }
    public void SwitchSelection()
    {

        switch (selectedPlayer) {
            case 0:
                Debug.Log("Switched to 1");
                playersPanel.SetActive(false);
                selectionImage.transform.position = playersImages[1].transform.position;
                selectedPlayer = 1;
                limbsPanel.SetActive(true);
                Invoke("Hide", 4f);
                break;
            case 1:
                Debug.Log("Switched to 0");
                limbsPanel.SetActive(false);
                selectionImage.transform.position = playersImages[0].transform.position;
                selectedPlayer = 0;
                playersPanel.SetActive(true);
                Invoke("Hide", 4f);
                break;
        }
    }

    void Hide()
    {
        if(selectedPlayer ==0)
            playersPanel.SetActive(false);
        else if(selectedPlayer == 1)
            limbsPanel.SetActive(false);
        Debug.Log("It works");
    }
}
