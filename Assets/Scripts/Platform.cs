﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    Vector3 posA;
    Vector3 posB;
    Vector3 nextPos;

    [SerializeField] private Transform childTransform;
    [SerializeField] private Transform transformB;
    [SerializeField] float speed;

    private void Start()
    {
        posA = childTransform.localPosition;
        posB = transformB.localPosition;
        nextPos = posB;    
    }

    public void Move()
    {
        childTransform.localPosition = Vector3.MoveTowards(childTransform.localPosition, nextPos, speed * Time.deltaTime);
    }

    public void ChangeDestination()
    {
        nextPos = nextPos != posA ? posA : posB;
    }
}
