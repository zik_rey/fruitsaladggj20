﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyButtonController : MonoBehaviour
{

    [SerializeField] Platform platform;
    [SerializeField] DoorController door;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" || other.tag == "Limb")
        {
            platform.Move();
            door.SwitchDoorState();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Limb")
        {
            platform.Move();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Limb")
        {
            platform.ChangeDestination();
            door.SwitchDoorState();
        }
    }
}
