﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_danger : MonoBehaviour
{
    [Header("Spawns point")]
    public Transform Spawn_player;
    public Transform Spawn_limb;

    [Header("Players")]
    public Transform Player;
    public Transform Limb;


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {            
            Player.position = Spawn_player.position;
        }
        if (col.gameObject.tag == "Limb")
        {
            Limb.position = Spawn_limb.position;
        }
    }
}
