﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private Trigger_plate Trigger_Plate;

    bool is_click = false;

    [SerializeField] float range = 0f;
    [SerializeField] float speed = 0f;


    private void Update()
    {
        Debug.DrawRay(this.gameObject.transform.localPosition,new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y + range, this.gameObject.transform.localPosition.z),Color.red);
        Debug.DrawRay(this.gameObject.transform.position, Trigger_Plate.transform.position, Color.green);
        if (Trigger_Plate.Is_pressed)
            is_click = true;

        if (is_click)
        {
            Vector3 new_pose = new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y + range, this.gameObject.transform.localPosition.z);
            this.gameObject.transform.localPosition = Vector3.MoveTowards(this.gameObject.transform.localPosition, new_pose, speed * Time.deltaTime);
        }
    }
}
