﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_plate : MonoBehaviour
{
    public float range = 0.001f;
    public bool Is_pressed;
    public Transform Button;

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Limb")
        {
            Is_pressed = true;

            Button.transform.localPosition = new Vector2(Button.transform.localPosition.x, Button.transform.localPosition.y - range);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Limb")
        {
            Is_pressed = false;

            Button.transform.localPosition = new Vector2(Button.transform.localPosition.x, Button.transform.localPosition.y + range);

        }
    }

}
