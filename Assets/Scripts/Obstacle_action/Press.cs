﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Press : MonoBehaviour
{   
    [SerializeField] private float time = 1f;
    [SerializeField] private float range = 1f;    


    Transform Start_Location,End_Location;
    bool is_up = true;
    void Start()
    {
        //Start_Location = 
        //End_Location = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + range, this.gameObject.transform.position.z);        
    }
    void Update()
    {
        if (is_up)
        {
            this.gameObject.transform.localPosition = Vector3.MoveTowards(Start_Location.localPosition, new Vector3(Start_Location.localPosition.x, Start_Location.localPosition.y + range, Start_Location.localPosition.z), time * Time.deltaTime);
            if (this.gameObject.transform.localPosition.y >= Start_Location.localPosition.y+range)
            is_up = false;
        }
        else
        {
            this.gameObject.transform.localPosition = Vector3.MoveTowards(new Vector3(Start_Location.localPosition.x, Start_Location.localPosition.y + range, Start_Location.localPosition.z), Start_Location.localPosition, time * Time.deltaTime);
            is_up = true;
        }
    }

}
