﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] public byte currentPlayer = 0;
    [SerializeField] public Player Main_player;
    [SerializeField] public Player Leg;
    [SerializeField] public Player Arm;

    public bool is_con = false;
    void Start()
    {
        GameObject.FindObjectOfType<HUD>().selectedPlayer = 0;
        currentPlayer = 1;
        GameObject.FindObjectOfType<CameraControl>().player = GameObject.FindGameObjectWithTag("Player").transform;
        Main_player.Is_control = true;
        Leg.Is_control = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (is_con)
        {
            if (Input.GetKeyDown(KeyCode.Tab) && currentPlayer == 0)
            {
                currentPlayer = 1;
                GameObject.FindObjectOfType<CameraControl>().player = GameObject.FindGameObjectWithTag("Player").transform;
                Main_player.Is_control = true;
                Leg.Is_control = false;
            }
            else if (Input.GetKeyDown(KeyCode.Tab) && currentPlayer == 1)
            {
                currentPlayer = 0;
                GameObject.FindObjectOfType<CameraControl>().player = GameObject.FindGameObjectWithTag("Limb").transform;
                Main_player.Is_control = false;
                Leg.Is_control = true;

            }
        }
    }
}
