﻿using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [SerializeField] private float _jumpSpeed = 40f;
    [SerializeField] public float Run_speed = 40f;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Animator _animator;

    [Header("VFX")]
    [Space]
    [SerializeField] private ParticleSystem _groundedParticleSystem;
    [SerializeField] private ParticleSystem _metalHitParticleSystem;
    [SerializeField] private ParticleSystem _steamParticleSystem;

    private bool m_Grounded;            // Whether or not the player is grounded.
    private Rigidbody m_Rigidbody;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    //[System.Serializable]
    //public class BoolEvent : UnityEvent<bool> { }



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(m_GroundCheck.position, k_GroundedRadius);
    }

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        if(_groundedParticleSystem) _groundedParticleSystem.Stop(true);
    }

    private void FixedUpdate()
    {
        JumpChecking();
    }

    private void JumpChecking()
    {
        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        Collider[] colliders = Physics.OverlapSphere(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
                if (!wasGrounded)
                {
                    OnLandEvent.Invoke();
                    _animator.SetBool("IsJumping", false);
                    if (_groundedParticleSystem) _groundedParticleSystem.Play(true);
                }
            }
        }

    }

    public void Move(float move)
    {

        move = move * Run_speed * Time.deltaTime;
        //only control the player if grounded or airControl is turned on
        if (m_Grounded || m_AirControl)
        {

            // Move the character by finding the target velocity 
            Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody.velocity.y);
            // And then smoothing it out and applying it to the character
            m_Rigidbody.velocity = Vector3.SmoothDamp(m_Rigidbody.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            _animator.SetFloat("MovingSpeed", Mathf.Abs(m_Rigidbody.velocity.x));

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
        // If the player should jump...
        
    }

    public void JumpRobot()
    {
        if (m_AirControl)
        {
            if (m_Grounded)
            {
                {
                    // Add a vertical force to the player.
                    m_Grounded = false;

                    //m_Rigidbody.AddForce(new Vector2(0f, m_JumpForce));
                    m_Rigidbody.velocity = Vector3.up * _jumpSpeed;
                    _animator.SetBool("IsJumping", true);

                }
            }
        }
    }

    public void Leg_move(float move)
    {
        // If the input is moving the player right and the player is facing left...
        if (move > 0 && !m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (move < 0 && m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }

        move = move * Run_speed;

        if (m_Grounded)
        {
            m_Rigidbody.AddForce(new Vector2(move, m_JumpForce));
            _animator.SetTrigger("IsJump");
        }
    }

   

    

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        //transform.Rotate(0f, 180f, 0f);
    }


    public void MetalHitPSPlay()
    {
        if (_metalHitParticleSystem) _metalHitParticleSystem.Play(true);
    }

    public void SteamPSPLay()
    {
        if(_steamParticleSystem) _steamParticleSystem.Play(true);
    }
}