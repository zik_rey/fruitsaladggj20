﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [Header("Deep dark camera")]
    public float deep = 10f;

    [Header("Other settings")]
    public float dumping = 1.5f;
    public Vector2 offset = new Vector2(2f, 1f);
    public bool isLeft;
    public Transform player;   
    private int lastX;

    [SerializeField] private Transform _playerPosition;
    [SerializeField] private Vector3 _cameraOffset = new Vector3(0, 0, -4);
    [SerializeField] private float _smoothSpeed = 0.125f;

    private void Awake()
    {
        _playerPosition = GameObject.FindGameObjectWithTag("Limb").transform;
    }


    private void Start()
    {
        offset = new Vector2(Mathf.Abs(offset.x), offset.y);
        //FindPlayer(isLeft);
    }

    private void LateUpdate()
    {
        if (player)
        {

            Vector3 desiredPos = _playerPosition.position + _cameraOffset;
            Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, _smoothSpeed);
            transform.position = smoothedPos;
            //int currentX = Mathf.RoundToInt(player.position.x);
            //if (currentX > lastX)
            //    isLeft = false;
            //else if (currentX < lastX)
            //    isLeft = true;
            //lastX = Mathf.RoundToInt(player.position.x);

            //Vector3 target = new Vector3();
            //if (isLeft)
            //{
            //    target = new Vector3(player.position.x - offset.x, player.position.y + offset.y, -deep);
            //}
            //else
            //{
            //    target = new Vector3(player.position.x + offset.x, player.position.y + offset.y, -deep);
            //}
            //Vector3 currentPosition = Vector3.Lerp(transform.position, target, dumping * Time.deltaTime);
            //transform.position = currentPosition;
        }
    }

    public void FindPlayer(bool playerIsLeft)
    {
        //player = GameObject.FindGameObjectWithTag("Limb").transform;
        //lastX = Mathf.RoundToInt(player.position.x);
        //if (playerIsLeft)
        //{
        //    transform.position = new Vector3(player.position.x - offset.x, player.position.y - offset.y, -deep);
        //}
        //else
        //{
        //    transform.position = new Vector3(player.position.x + offset.x, player.position.y + offset.y, -deep);
        //}
    }
}
