﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public CharacterController2D Controller_Character;    

    [Header("Controls")]
    public bool Is_control = false;    
    public bool Is_leg = false;
    public bool Can_jump = true;
    public Animator animator;

    float Hor_move = 0f;
    bool Jump = false;
    private int Is_level=1;
    public State state = State.Idle;

    private void Update()
    {
        Hor_move = Input.GetAxisRaw("Horizontal");
        if (Is_control&& Can_jump)
        {
            Jumping();
        }

    }

    void FixedUpdate()
    {
        if (Is_control)
        {
            if (Hor_move == 0)
            {
                state = State.Idle;
            }
            else
            {
                state = State.Walk;
            }          
            
        }
        if (Is_control)
        {
            if (!Is_leg)
            {
                Moving();

            }
            
            //Jumping();
        }
    }

    private void Jumping()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Is_leg)
            {
                Controller_Character.Leg_move(Hor_move);
            }
            else
            {
                Controller_Character.JumpRobot();
            }
        }
        
    }

    private void Moving()
    {
        
        
            state = State.Jump;
            Controller_Character.Move(Hor_move);
            //Controller_Character.JumpRobot();
        
    }

   
    public enum State
    {
        Idle,
        Jump,
        Walk
    }
}
